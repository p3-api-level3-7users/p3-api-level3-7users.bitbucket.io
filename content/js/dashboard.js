/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.8346007604562737, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "getStaffHealthRecords"], "isController": false}, {"data": [1.0, 500, 1500, "getCentreHolidaysOfYear"], "isController": false}, {"data": [0.10585585585585586, 500, 1500, "getPastChildrenData"], "isController": false}, {"data": [1.0, 500, 1500, "suggestProgram"], "isController": false}, {"data": [1.0, 500, 1500, "findAllWithdrawalDraft"], "isController": false}, {"data": [1.0, 500, 1500, "findAllClass"], "isController": false}, {"data": [1.0, 500, 1500, "findAllLevels"], "isController": false}, {"data": [1.0, 500, 1500, "getStaffCheckInOutRecords"], "isController": false}, {"data": [1.0, 500, 1500, "getChildrenToAssignToClass"], "isController": false}, {"data": [1.0, 500, 1500, "getCountStaffCheckInOut"], "isController": false}, {"data": [0.0, 500, 1500, "searchBroadcastingScope"], "isController": false}, {"data": [1.0, 500, 1500, "getTransferDrafts"], "isController": false}, {"data": [1.0, 500, 1500, "registrationByID"], "isController": false}, {"data": [1.0, 500, 1500, "leadByID"], "isController": false}, {"data": [1.0, 500, 1500, "getRegEnrolmentForm"], "isController": false}, {"data": [0.09641255605381166, 500, 1500, "findAllLeads"], "isController": false}, {"data": [0.5, 500, 1500, "getEnrollmentPlansByYear"], "isController": false}, {"data": [1.0, 500, 1500, "getAvailableVacancy"], "isController": false}, {"data": [1.0, 500, 1500, "getRegistrations"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 4471, 0, 0.0, 430.68843659136763, 2, 4620, 33.0, 1651.0, 3200.999999999982, 4256.28, 14.788329424839498, 92.12100474166729, 29.991544402198898], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getStaffHealthRecords", 447, 0, 0.0, 5.058165548098431, 2, 29, 4.0, 8.0, 13.0, 19.519999999999982, 1.5136841107190508, 0.8913589050425661, 1.8680173066243153], "isController": false}, {"data": ["getCentreHolidaysOfYear", 223, 0, 0.0, 41.57399103139014, 32, 152, 37.0, 46.0, 73.19999999999976, 146.79999999999995, 0.7550670756895489, 1.9287719441368871, 0.9696418013005437], "isController": false}, {"data": ["getPastChildrenData", 222, 0, 0.0, 1636.81981981982, 1173, 2030, 1653.5, 1863.2, 1921.75, 1999.93, 0.7478826834839205, 1.4976406006811795, 1.7053770175146037], "isController": false}, {"data": ["suggestProgram", 224, 0, 0.0, 11.736607142857146, 8, 45, 10.0, 16.5, 20.75, 38.75, 0.7666060910960757, 0.6213701714938894, 0.9515198650225704], "isController": false}, {"data": ["findAllWithdrawalDraft", 222, 0, 0.0, 5.792792792792791, 3, 23, 5.0, 8.700000000000017, 15.0, 21.54000000000002, 0.7559943198264618, 0.4894767910595158, 2.468055674980334], "isController": false}, {"data": ["findAllClass", 225, 0, 0.0, 45.355555555555576, 31, 208, 39.0, 53.80000000000001, 84.79999999999995, 190.9600000000005, 0.7658192734587461, 76.27754409949013, 0.6192366781482831], "isController": false}, {"data": ["findAllLevels", 224, 0, 0.0, 9.745535714285717, 7, 38, 8.0, 15.5, 19.0, 28.5, 0.762371519978218, 0.485416241236131, 0.6387839493567491], "isController": false}, {"data": ["getStaffCheckInOutRecords", 223, 0, 0.0, 5.829596412556055, 3, 50, 4.0, 7.0, 15.0, 43.95999999999981, 0.7557477497017676, 0.44798719147360644, 0.9889667818362976], "isController": false}, {"data": ["getChildrenToAssignToClass", 225, 0, 0.0, 8.82666666666666, 5, 63, 8.0, 12.0, 17.0, 25.74000000000001, 0.7579304792478635, 0.4315170599624067, 0.5558650292140093], "isController": false}, {"data": ["getCountStaffCheckInOut", 224, 0, 0.0, 5.2232142857142865, 3, 22, 5.0, 6.0, 10.0, 20.0, 0.7588461473313346, 0.44834171790572014, 0.7736673611463997], "isController": false}, {"data": ["searchBroadcastingScope", 227, 0, 0.0, 3983.6035242290754, 3064, 4620, 3982.0, 4359.6, 4484.0, 4585.16, 0.7588344036129878, 1.470176366319787, 1.487285789112565], "isController": false}, {"data": ["getTransferDrafts", 224, 0, 0.0, 5.58482142857143, 3, 34, 5.0, 7.0, 14.0, 20.5, 0.7586431126043134, 0.48748746884144356, 1.3165125108377587], "isController": false}, {"data": ["registrationByID", 224, 0, 0.0, 48.57142857142862, 38, 130, 46.0, 56.0, 63.75, 109.25, 0.7578961614589501, 1.268987714850028, 3.447539375074013], "isController": false}, {"data": ["leadByID", 221, 0, 0.0, 190.23076923076934, 138, 247, 193.0, 211.0, 215.79999999999995, 244.9, 0.7642378197436173, 2.0747795143631755, 3.4510114047797713], "isController": false}, {"data": ["getRegEnrolmentForm", 224, 0, 0.0, 37.39732142857141, 28, 72, 35.0, 46.5, 49.0, 63.75, 0.7573990106475423, 1.3595942262273075, 3.2381766295067775], "isController": false}, {"data": ["findAllLeads", 223, 0, 0.0, 1623.5695067264576, 1241, 2010, 1635.0, 1807.0, 1841.3999999999999, 1944.9599999999998, 0.7429767045151661, 1.487801480289461, 1.681855469791167], "isController": false}, {"data": ["getEnrollmentPlansByYear", 225, 0, 0.0, 807.3600000000002, 527, 959, 816.0, 869.4, 891.0999999999999, 953.96, 0.7721901715634963, 0.8572048238720017, 1.281202247545293], "isController": false}, {"data": ["getAvailableVacancy", 222, 0, 0.0, 10.648648648648647, 7, 39, 9.0, 17.400000000000034, 19.0, 33.39000000000007, 0.7545015191990049, 0.4133548362017986, 0.9983882407369645], "isController": false}, {"data": ["getRegistrations", 222, 0, 0.0, 76.75675675675676, 61, 162, 75.0, 88.0, 92.0, 124.94000000000023, 0.7556109216411052, 1.783035428621997, 2.3332438810831784], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 4471, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
